  <!-- Begin footer -->
        <footer id="footer" class="row">
            <div class="col-md-10 col-md-offset-1 no-padding">
                <!-- Social networks -->
                <ul class="social-networks clearfix">
                    <li class="facebook-icon">
                        <a href="albums.html#" target="_blank"> <!-- set your facebook profile -->
                            <i class="fa fa-facebook white" aria-hidden="true"></i> <!-- facebook image -->
                        </a>
                    </li>
                    <li class="instagram-icon">
                        <a href="albums.html#" target="_blank"> <!-- set your instagram profile -->
                            <i class="fa fa-instagram white" aria-hidden="true"></i> <!-- instagram image -->
                        </a>
                    </li>
                    <li class="500px-icon">
                        <a href="albums.html#" target="_blank"> <!-- set your 500px profile -->
                            <i class="fa fa-google-plus white" aria-hidden="true"></i> <!-- 500px image -->
                        </a>
                    </li>
                    <li class="flickr-icon">
                        <a href="albums.html#" target="_blank"> <!-- set your flickr profile -->
                            <i class="fa fa-flickr white" aria-hidden="true"></i> <!-- flick image -->
                        </a>
                    </li>                            
                </ul>
                <div class="copyright">
                    <p style="color:#fff;"> Copyright © 2017. Build by : <a href="http://sunztech.com/" title="SunzTech" target="_blank" style="color: #fff; font-weight: bold;">Sunztech</a></p>
                </div>
            </div>
        </footer>
        <!-- end of the footer -->
    </div>

    <?php if(isset($js)){
     foreach($js as $script) { ?>
     <script type="text/javascript" src="<?=$script?>"></script>
    <?php }} ?>

    <script type="text/javascript" src="<?php echo base_url; ?>assets/js/jquery-3.1.1.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script type="text/javascript" src="<?php echo base_url; ?>assets/js/pace.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url; ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url; ?>assets/js/magnific-popup.js"></script>
    <script type="text/javascript" src="<?php echo base_url; ?>assets/js/unslider.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url; ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url; ?>assets/js/main.js"></script>
   

    <script type="text/javascript">
        
     $("#divid").click(function() {
    $(this).select();
});

    </script>
</body>
</html>