<?php
session_start();
include 'config/config.php';
include 'liberaries/database.php';
include 'helpers/format_helper.php';
/*if(isset($_SESSION['id'])){
    
    $id = $_SESSION['id'];
    
}else{
    
    header('location:index.php');
} */
ob_start();
$db= new database();
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en">
<![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="bg">
<!--<![endif]--><head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title>Salveo world</title>

    <meta name="description" content="Bebovski Photography Site template">
    <meta name="Keywords" lang="en" content="Bebovski Photography Site template" xml:lang="en">
    <meta name="robots" content="all">

    <link href="albums.html#" rel="shortcut icon">
    <meta name="author" content="Monny Design">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

    <link type="text/css" href="<?php echo base_url; ?>assets/css/bootstrap.min.css" rel="stylesheet" media="screen"/> 
    <link type="text/css" href="<?php echo base_url; ?>assets/css/bootstrap-social.css" rel="stylesheet" media="screen"/>
    <link type="text/css" href="<?php echo base_url; ?>assets/css/pace.css" rel="stylesheet" />
    <link type="text/css" href="<?php echo base_url; ?>assets/css/magnific-popup.css" rel="stylesheet"/> 
    <!-- Font awsome -->
    <link type="text/css" href="<?php echo base_url; ?>assets/css/font-awesome-4.6.3/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" id="fontawesome-style-css" media="all">
    <link type="text/css" href="<?php echo base_url; ?>assets/css/jquery.mCustomScrollbar.css" rel="stylesheet"/>
    <link type="text/css" href="<?php echo base_url; ?>assets/css/style_front.css" rel="stylesheet"  media="screen">
    <link type="text/css" href="<?php echo base_url; ?>assets/includes/fonts.googleapis.com/css.css" rel="stylesheet">
 
</head>

<body class="bg-set">
    <div class="container-fluid" >
        <header class="top-menu" >
            <nav class="navbar navbar-default" style="background-color: #5d7a03;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 no-padding">
                            <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a href="<?php echo base_url;?>" class="navbar-brand" style="width:200px;"> <!-- set your facebook profile -->
                                  <img src="<?php echo base_url;?>/assets/images/logo_white_plus.png" style="width:100%" class="img-responsive"/>
                                </a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right menu">
                        <?php  
                        
                        $current_file_name = basename($_SERVER['PHP_SELF']);
                        $phot = "SELECT * FROM categories WHERE parent_id = 0";
                        $phots = $db->select($phot);
                        if($phots || $phots->num_rows != 0){
                        while($menu = $phots->fetch_assoc()){
                        ?> 
                                    <li <?php if( $menu['page'] == $current_file_name){ echo 'class="active"';}?>><a href="<?php echo base_url.$menu['page']; ?>" style=""><?php echo $menu['categories']; ?></a></li>
                        <?php  }
                        } ?>
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </div>
                </div>
            </nav>
        </header>