<?php include 'includes/header.php'; ?>

<section id="content-wrap" class="row">
            <div class="col-md-10 col-md-offset-1 fullheight">
  
                <!-- Beginning of about section -->
                <main class="albums content setheight mayScroll row" data-mcs-theme="dark">
                    <div class="col-md-12">
                        <div class="row">
<?php
$padding = 0;
$phot = "SELECT * FROM categories WHERE parent_id = 1";
$phots = $db->select($phot);
while($photo_cats = $phots->fetch_assoc()){

    $sub = $photo_cats['category_id'];
     $recent_pic = "SELECT * FROM photos WHERE sub_category_id = '$sub' order by photo_id desc limit 1;";
     $grab_pic = $db->select($recent_pic);
    
     if(!$grab_pic || $grab_pic->num_rows == 0){
             
?>
   <div class="album-set col-md-6 col-xs-12 <?php if($padding >= 2){ echo "padding-top";} ?>">
                                <a href="<?php echo profileLink($photo_cats['categories']); ?>">
                                    <img src="<?php echo base_url; ?>assets\images\image-not-available.jpg" alt="Title" title="<?php echo $photo_cats['categories'];  ?>" class="img-responsive" style="min-height: 365px !important;max-height: 365px !important;/*min-width: 613px !important;*/width: 100%;"/>
                                </a>
                                <div class="album-title">
                                    <h2><?php echo $photo_cats['categories']; ?></h2>                            
                                </div>
                            </div>

<?php
  }else{
                    $pic = $grab_pic->fetch_assoc();
?>

                          <div class="album-set col-md-6 col-xs-12 <?php if($padding >= 2){ echo "padding-top";} ?>">
                                <a href="<?php echo profileLink($photo_cats['categories']); ?>">
                                    <img src="<?php echo base_url; ?>admin\uploads\photos\<?php echo $pic['photo_name']; ?>" alt="Title" title="<?php echo $photo_cats['categories'];  ?>" class="img-responsive" style="min-height: 365px !important;max-height: 365px !important;/*min-width: 613px !important;*/width: 100%;"/>
                                </a>
                                <div class="album-title">
                                    <h2><?php echo $photo_cats['categories']; ?></h2>                            
                                </div>
                            </div>

<?php
  }
  $padding = $padding + 1;
}
?>
                        </div>
                    </div>
                </main>
            </div>
        </section>
<?php include 'includes/footer.php'; ?>    