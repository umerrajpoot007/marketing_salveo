<?php
class database{
	
	public $host = HOST;
	public $username = DB_USER;
	public $password = DB_PASS;
	public $db_name = DB_NAME;
	
	public $error;
	public $concern;
	
public function __construct(){
	
	$this->connect();
}
	
private function connect (){
	
	$this->concern = New mysqli($this -> host,$this -> username,$this -> password,$this -> db_name );
	
	if(!$this->concern){
		
	$this->error = "Sorry Could not connect to the database".$this->concern->connect_error;
		return false;
				
	}
	
	
}

/*
 *the select function
 */

public function select($query){
	
	$result = $this->concern->query($query)or die($this->concern->error. __LINE__);
	
	if($result->num_rows > 0){
		
		Return $result;
		
	}else{
		
	return false;	 
	}
	
}

/*
 *the insert function
 */

public function insert($query){
	
	$inserted_row = $this->concern->query($query) or die($this->concern->error . __LINE__);
	
	if($inserted_row){
		
		return "Data Has Been Updated";
		
	}else{
		
		die('Error:('.$this->concern->errno.')'.$this->concern->error);
	}
	
}

/*
 *the update function
 */

public function update($query){
	
	$updated_row = $this->concern->query($query) or die($this->concern->error . __LINE__);
	
	if($updated_row){
		
		return "The Data has been deleted";
		
	}else{
		
	die('Error:('.$this->concern->errno.')'.$this->concern->error);
	}
	
}

/*
 *the delete function
 */

public function del($query){
	
	$deleted_row = $this->concern->query($query) or die($this->concern->error . __LINE__);
	
	if($deleted_row){
		
		return "The Data has been deleted";
		
	}else{
		
		die('Error:('.$this->concern->errno.')'.$this->concern->error);
	}
	
}

	
	
}




?>