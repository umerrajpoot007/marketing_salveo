<?php 

if(!empty($_GET['red']) and $_GET['red'] == true) {
    header('location: http://salveoworld.com/');
}

session_start();
include 'config/config.php';
include 'liberaries/database.php';
include 'helpers/format_helper.php';

ob_start();
$db= new database();

if(isset($_GET['pic_id'])){
    
$pic_id = $_GET['pic_id'];
    
  $single_pic = "SELECT * FROM photos WHERE photo_id = '$pic_id'";
  $grab_pic = $db->select($single_pic);
  $single = $grab_pic->fetch_assoc();
}else{
    header('location:index.php');
}
?>
<!DOCTYPE html>

<html class="no-js" lang="bg">
<!--<![endif]--><head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title><?php echo $single['caption']; ?></title>

    <meta name="description" content="">
    <meta name="Keywords" lang="en" content="" xml:lang="en">
    <meta name="robots" content="all">

    <meta property="og:image" content="<?php echo base_url; ?>admin/uploads/photos/<?php echo $single['photo_name']; ?>" />
    <meta property="og:description" content="<?php echo $single['caption']; ?>" />
    <meta property="og:url" content="http://salveoworld.com/" />
    <meta property="og:title" content="<?php echo $single['caption']; ?>" />

    <link href="albums.html#" rel="shortcut icon">
    <meta name="author" content="Monny Design">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

    <link type="text/css" href="<?php echo base_url; ?>assets/css/bootstrap.min.css" rel="stylesheet" media="screen"/> 
    <link type="text/css" href="<?php echo base_url; ?>assets/css/bootstrap-social.css" rel="stylesheet" media="screen"/>
    <link type="text/css" href="<?php echo base_url; ?>assets/css/pace.css" rel="stylesheet" />
    <link type="text/css" href="<?php echo base_url; ?>assets/css/magnific-popup.css" rel="stylesheet"/> 
    <!-- Font awsome -->
    <link type="text/css" href="<?php echo base_url; ?>assets/css/font-awesome-4.6.3/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" id="fontawesome-style-css" media="all">
    <link type="text/css" href="<?php echo base_url; ?>assets/css/jquery.mCustomScrollbar.css" rel="stylesheet"/>
    <link type="text/css" href="<?php echo base_url; ?>assets/css/style_front.css" rel="stylesheet"  media="screen">
    <link type="text/css" href="<?php echo base_url; ?>assets/includes/fonts.googleapis.com/css.css" rel="stylesheet">
</head>

<body class="bg-set">
    <div class="container-fluid" >
        <header class="top-menu">
            <nav class="navbar navbar-default" style="background-color: #5d7a03;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-10 no-padding">
                            <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a href="<?php echo base_url;?>" class="navbar-brand" style="width:200px;"> <!-- set your facebook profile -->
                                  <img src="<?php echo base_url;?>/assets/images/logo_white_plus.png" style="width:100%" class="img-responsive"/>
                                </a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right menu">
                        <?php  
                        
                        $current_file_name = basename($_SERVER['PHP_SELF']);
                        $phot = "SELECT * FROM categories WHERE parent_id = 0";
                        $phots = $db->select($phot);
                        if($phots || $phots->num_rows != 0){
                        while($menu = $phots->fetch_assoc()){
                        ?> 
                                    <li <?php if( $menu['page'] == $current_file_name){ echo 'class="active"';}?>><a href="<?php echo base_url.$menu['page']; ?>" style=""><?php echo $menu['categories']; ?></a></li>
                        <?php  }
                        } ?>
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <section id="content-wrap" class="row">
            <div class="col-md-10 col-md-offset-1 fullheight">
                <!-- Beginning of about section -->
                <main class="albums content mayScroll setheight row" data-mcs-theme="dark" >
                    <div class="col-md-12">
                        <div class="row">
                            <ul class="gallery-container list-unstyled">
                                <?php
                        if(!$grab_pic || $grab_pic->num_rows == 0){
                                ?>
                                <li class="album-set-img col-md-8">
                                    <a href="<?php echo base_url; ?>assets/images/image-not-available.jpg">
                                        <img src="<?php echo base_url; ?>assets/images/image-not-available.jpg" alt="Title" title="Title" class="img-responsive" style="max-height: 500px;
    margin: 0 auto;"/> 
                                    </a>
                                </li>
                                 
                                <?php
                                  }else{
                                    ?>
                                <li class="album-set-img col-md-8" style="float: none !important;margin:0 auto !important;">
                                    <h3 style="text-align:center;"><?php echo $single['caption']; ?></h3>
                                    <a href="<?php echo base_url; ?>admin/uploads/photos/<?php echo $single['photo_name']; ?>">
                                        <img src="<?php echo base_url; ?>admin/uploads/photos/<?php echo $single['photo_name']; ?>" alt="Title" title="<?php $single['caption']; ?>" class="img-responsive" style="max-height: 500px;
    margin: 0 auto;"/> 
                                    </a>

                                    <div class="btn btn-block btn-facebook share" onclick="testFunction(this)" name ="<?php echo base_url; ?>admin/uploads/photos/<?php echo $single['photo_name']; ?>" style="width: 20%;margin-top: 1%;font-size: 10px;    margin: 4% auto;">
                                    <span class="fa fa-facebook "></span> Share on facebbok
                                    </div>
                                </li>
                            <?php
                              }
                                ?>
                            </ul>
                        </div>
                    </div>
                </main>
            </div>
        </section>
        <script type="text/javascript">

function testFunction (e){
    var href = document.URL;//e.getAttribute('name');
    var photo_path = e.getAttribute('name');
   // window.location = "https://www.facebook.com/dialog/share?app_id=1832681676761220&display=popup&href=salveoworld.com&redirect_uri="+href+"picture="+photo_path; 

   window.location = "https://www.facebook.com/sharer/sharer.php?u="+href;

    
}
        </script>
<?php include 'includes/footer.php'; ?>  