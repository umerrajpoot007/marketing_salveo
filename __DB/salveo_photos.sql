-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2017 at 07:06 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salveo_photos`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `banner_id` int(11) NOT NULL,
  `banner_name` varchar(50) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`banner_id`, `banner_name`, `width`, `height`) VALUES
(2, 'banner_1505901049.png', 432, 151),
(3, 'banner_1505906678.JPG', 222, 333),
(4, 'banner_1505908512.png', 500, 600),
(6, 'banner_1506080109.jpg', 450, 500),
(7, 'banner_1506081741.jpg', 12000, 213333),
(8, 'banner_29793.jpg', 12, 321);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `categories` varchar(30) CHARACTER SET utf8 NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `slug` varchar(50) NOT NULL,
  `page` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `categories`, `parent_id`, `slug`, `page`) VALUES
(1, 'Photos', 0, 'photos', 'index.php'),
(2, 'Videos', 0, 'videos', 'videos.php'),
(3, 'Banners', 0, 'banners', 'banners.php'),
(4, 'Products', 1, 'products', NULL),
(5, 'Opportunity', 1, 'opportunity', NULL),
(6, 'Proof of Income', 1, 'proof-of-Income', NULL),
(7, 'Proof of Shipment', 1, 'proof-of-shipment', NULL),
(8, 'Testimonial Products', 1, 'testimonial-products', NULL),
(9, 'Testimonial Opportunity', 1, 'testimonial-opportunity', NULL),
(10, 'Online Shopping', 1, 'online-shopping', NULL),
(11, 'Others', 1, 'others', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `panel_user_info`
--

CREATE TABLE `panel_user_info` (
  `id` int(11) NOT NULL,
  `full_name` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(30) NOT NULL,
  `user_type` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `panel_user_info`
--

INSERT INTO `panel_user_info` (`id`, `full_name`, `email`, `password`, `user_type`) VALUES
(1, 'umer hayat', 'umer54321@yahoo.com', 'umer888', 'Agent'),
(2, 'umair', 'malikwaheedkhan@hotmail.com', 'umair123', 'Agent'),
(3, 'Mateen Ahmed', 'mateen@yahoo.com', 'mateen123', 'Admin'),
(4, 'malikwaheed', 'malikwaheedkhan@hotmail.com', 'waheed12', 'Admin'),
(5, 'athar', 'athargoraya.sharif@gmail.com', 'aliathar', 'Agent');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `photo_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `caption` varchar(50) NOT NULL,
  `photo_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`photo_id`, `sub_category_id`, `category_id`, `type_id`, `caption`, `photo_name`) VALUES
(2, 5, 1, 2, 'The test photo', 'photo_1505725136.jpg'),
(5, 4, 1, 2, 'The test products', 'photo_1505917436.jpg'),
(6, 6, 1, 1, 'The test proof', 'photo_1505917459.jpg'),
(7, 7, 1, 1, 'The test shipment proof', 'photo_1505917492.JPG'),
(8, 9, 1, 1, 'The testimonial oppertunity', 'photo_1505917531.jpg'),
(10, 8, 1, 1, 'The testimonial products', 'photo_1505918883.jpg'),
(11, 10, 1, 2, 'online shopping', 'photo_1505918909.jpg'),
(12, 6, 1, 2, 'The new proof', 'photo_1505991013.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `photo_types`
--

CREATE TABLE `photo_types` (
  `photo_type_id` int(11) NOT NULL,
  `type_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photo_types`
--

INSERT INTO `photo_types` (`photo_type_id`, `type_name`) VALUES
(1, 'Facebook covers'),
(2, 'Facebook posts'),
(3, 'Square'),
(4, 'Rectangle');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `video_id` int(11) NOT NULL,
  `video_caption` varchar(200) NOT NULL,
  `embed_code` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`video_id`, `video_caption`, `embed_code`) VALUES
(4, 'The video of marketing', '<iframe width="560" height="315" src="https://www.youtube.com/embed/hZLMv5aexto" frameborder="0" allowfullscreen></iframe>'),
(6, 'Science of persuation', '<iframe width="560" height="315" src="https://www.youtube.com/embed/cFdCzN7RYbw" frameborder="0" allowfullscreen></iframe>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `panel_user_info`
--
ALTER TABLE `panel_user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`photo_id`);

--
-- Indexes for table `photo_types`
--
ALTER TABLE `photo_types`
  ADD PRIMARY KEY (`photo_type_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `panel_user_info`
--
ALTER TABLE `panel_user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `photo_types`
--
ALTER TABLE `photo_types`
  MODIFY `photo_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
