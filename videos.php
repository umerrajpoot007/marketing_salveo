<?php include 'includes/header.php'; ?>

<?php 
ob_start();
$db= new database();

  $all_vids = "SELECT * FROM videos";
  $grab_vids = $db->select($all_vids);
?>
        <section id="content-wrap" class="row">
            <div class="col-md-10 col-md-offset-1 fullheight">
                <!-- Beginning of about section -->
                <main class="albums content mayScroll setheight row" data-mcs-theme="dark">
                    <div class="col-md-12">
                        <div class="row">
                            <ul class="gallery-container list-unstyled">
                                <?php
                        if(!$grab_vids || $grab_vids->num_rows == 0){
                                ?>
                                <li class="album-set-img col-md-6">
                                    <a href="<?php echo base_url; ?>assets/images/image-not-available.jpg">
                                        <img src="<?php echo base_url; ?>assets/images/image-not-available.jpg" alt="Title" title="Title" class="img-responsive"/> 
                                    </a>
                                </li>
                                 <li class="album-set-img col-md-6">
                                    <a href="<?php echo base_url; ?>assets/images/image-not-available.jpg">
                                        <img src="<?php echo base_url; ?>assets/images/image-not-available.jpg" alt="Title" title="Title" class="img-responsive"/> 
                                    </a>
                                </li>
                                <?php
                                  }else{
                                         while($single_pic = $grab_vids->fetch_assoc()):
                                    ?>
                                <li class="album-set-img col-md-6">
                                    <h3><?php echo $single_pic['video_caption']; ?></h3>
                                    <a href="<?php echo base_url; ?>admin/uploads/photos/<?php echo $single_pic['photo_name']; ?>">
                                        <!--<img src="<?php //echo base_url; ?>admin/uploads/photos/<?php //echo $single_pic['photo_name']; ?>" alt="Title" title="<?php //$single_pic['caption']; ?>" class="img-responsive"/>-->
                                       <?php echo $single_pic['embed_code']; ?>
                                    </a>
                                </li>
                            <?php
                            endwhile;
                              }
                                ?>
                            </ul>
                        </div>
                    </div>
                </main>
            </div>
        </section>
<?php include 'includes/footer.php'; ?>  