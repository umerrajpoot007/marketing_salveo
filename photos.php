<?php include 'includes/header.php'; ?>

<?php 
ob_start();
$db= new database();

if(isset($_GET['title'])){
    
$title= $_GET['title'];
    
} 

$pic_cat = "SELECT * FROM categories WHERE slug = '$title'";
 $result = $db->select($pic_cat);

if(!$result || mysqli_num_rows($result) == 0){
    
   echo "<h1>No such picture category available.</h1>";
    exit;
 }else{
 $array = mysqli_fetch_array($result);
 $subcat_id = $array['category_id'];

  $all_pics = "SELECT * FROM photos WHERE sub_category_id = '$subcat_id'";
  $grab_pic = $db->select($all_pics);
 }

?>
        <section id="content-wrap" class="row">
            <div class="col-md-10 col-md-offset-1 fullheight">
                <!-- Beginning of about section -->
                <main class="albums content mayScroll setheight row" data-mcs-theme="dark">
                    <div class="col-md-12">
                        <div class="row">
                            <ul class="gallery-container list-unstyled">
                                <?php
                        if(!$grab_pic || $grab_pic->num_rows == 0){ ?>
                                <?php
                                  }else{
                                         while($single_pic = $grab_pic->fetch_assoc()):
                                    ?>
                                <li class="album-set-img col-md-6">
                                    <h3><?php echo $single_pic['caption']; ?></h3>
                                    <a href="<?php echo base_url; ?>admin/uploads/photos/<?php echo $single_pic['photo_name']; ?>">
                                        <img src="<?php echo base_url; ?>admin/uploads/photos/<?php echo $single_pic['photo_name']; ?>" alt="Title" title="<?php $single_pic['caption']; ?>" class="img-responsive" style="max-height:500px; margin: 0 auto !important;"/> 
                                    </a>

                                    <div class="btn btn-block btn-openid share" onclick="testFunction(this)" name ="<?php echo $single_pic['photo_id']; ?>" style="width: 15%;margin-top: 1%;font-size: 10px;background-color: #E8AA30!important;">
                                   <span class="fa fa-share "></span> Share picture

                                    </div>
                                </li>
                            <?php
                            endwhile;
                              }
                                ?>
                            </ul>
                        </div>
                    </div>
                </main>
            </div>
        </section>

<script type="text/javascript">
        function testFunction(e){

            var id = e.getAttribute('name');
          href = "http://localhost/sun/salveo_marketing/photo_share.php?pic_id="+id;
          window.location = href;
    }
</script>
<?php include 'includes/footer.php'; ?>  