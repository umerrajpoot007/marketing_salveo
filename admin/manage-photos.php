<?php
session_start();
include '../config/config.php';
include '../liberaries/database.php';
ob_start();
$db= new database();

if(isset($_SESSION['id'])){
	
	$id = $_SESSION['id'];
	
}else{
	
	header('location:index.php');
}
include 'includes/header.php';
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage photos
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Manage Stores</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
         
          <div class="row">
           <?php if(isset($_GET['msg'])):?> <P id="message"> <?php echo $_GET['msg'];
                  echo ('<meta http-equiv="refresh" content="3;url=manage-photos.php">');
           ?>  </P> <?php endif;?>
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				
                 <form action="manage-photos.php" method="post">
                  <div class="box-body">
                   <div class="form-group" >
                   
                   
                      <div class="box-tools">
				 
                    <!--<div class="input-group" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button type="submit" name="search_store" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>-->
                <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">

                    <label>Select Category</label>
                      <select class="form-control select2" name="cate" style="width: 100%;">
                      <?php 
                        $get = "SELECT * FROM categories where parent_id = 1 ORDER BY  categories ASC";

                         $options = $db->select($get);
                         while($array = $options -> fetch_assoc()) :
                      ?>      
                         <option value="<?php echo $array['category_id'];  ?>"><?php echo $array['categories'];  ?></option>
                      <?php
                      endwhile;
                      ?>                      
                      </select>
                 
                    </div>
				           </div>

                     <div class="col-md-8">
                        <div class="form-group" style="    margin-top: 3%;">
                          <button type="submit" name="category_search" class="btn btn-primary">search</button>
                      </div>
                     </div>

                  </div>

                </div>
				   </form>
                </div><!-- /.box-header -->


	
 <div class="box-body table-responsive no-padding">
				 
				   <table class="table table-hover">
                    <tr>
                      <th>Photo Type</th>
                      <th>Photo Caption</th>
                      <th>Photo</th>
                      <th></th>
                    </tr>
                  
         
         <?php
		 
		 		 
$per_page = 10;
if (isset($_GET["page"])) {

$page = $_GET["page"];

}

else {

$page=1;

}

// Page will start from 0 and Multiple by Per Page
$start_from = ($page-1) * $per_page;	 
	
	if(isset($_POST['category_search'])){

    $category = $_POST['cate'];

    $table = "SELECT * FROM photos 
    INNER JOIN categories ON photos.sub_category_id = categories.category_id
    INNER JOIN photo_types ON photo_types.photo_type_id = photos.type_id WHERE photos.sub_category_id = '$category' LIMIT $start_from, $per_page";
  }else{

		$table = "SELECT * FROM photos 
    INNER JOIN categories ON photos.sub_category_id = categories.category_id
    INNER JOIN photo_types ON photo_types.photo_type_id = photos.type_id  LIMIT $start_from, $per_page";
}
	$results      = $db ->select($table);
		
		if(!$results || $results->num_rows == 0){
echo "<tr>";
      echo "<td> No data available </td>";
echo "</tr>";    
    }else{
			
		   while($row =	$results->fetch_assoc()){
			?>

           <tr>
                      <td><a href="edit-store.php?id=<?php echo $row['photo_id'];	 ?>" style="text-decoration:none;"><?php echo $row['type_name']; ?></a></td>
                       <td><?php echo $row['caption']; ?></td>
					             <td> <img src="<?php echo base_url; ?>admin/uploads/photos/<?php echo $row['photo_name']; ?>" width='60px' height = '60px'"/></td>
             
              <td><a href="delete.php?photo_id=<?php echo $row['photo_id'];	 ?>" style="text-decoration:none;"><span class="label label-danger">Delete</span></a></td> 
                       </tr>
<?php
 }
}
?>
               
                  </table>
				 
                  
                </div><!-- /.box-body -->
		
				
              </div><!-- /.box -->
            </div>	
			
<?php

//Now select all from table
$query = "SELECT * FROM photos 
    INNER JOIN categories ON photos.sub_category_id = categories.category_id
    INNER JOIN photo_types ON photo_types.photo_type_id = photos.type_id";
$result = $db->select($query);

if(!$result || $result->num_rows == 0){

}else{
// Count the total records
$total_records = $result->num_rows;

//Using ceil function to divide the total records on per page
$total_pages = ceil($total_records / $per_page);

//Going to first page
echo "<center> <ul class='pagination'>

<li><a href='manage-photos.php?page=1'>".'First Page'."</a></li> ";

for ($i=1; $i<=$total_pages; $i++) {

echo "<li><a href='manage-photos.php?page=".$i."'>".$i."</a></li> ";
};
// Going to last page
echo "<li><a href='manage-photos.php?page=$total_pages'>".'Last Page'."</a></li></center> ";
}
?>

</div>
    


	</div>
		  
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>
