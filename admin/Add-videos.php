<?php
session_start();
include '../config/config.php';
include '../liberaries/database.php';
ob_start();
$db= new database();

if(isset($_SESSION['id'])){
	
	$id = $_SESSION['id'];
	
}else{
	
	header('location:index.php');
}
include 'includes/header.php';
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Add New Video
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Coupons</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
           
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                 
                                  <div class="box box-primary">
                                  
                                   <div class="box-header with-border">
                  
                  <?php if(isset($_GET['msg'])):?> <P id="message"> <?php echo $_GET['msg'];                         echo ('<meta http-equiv="refresh" content="3;url=Add-videos.php">');
                  ?>
                  </P> <?php endif;?>

                  <?php if(isset($_GET['error'])):?> <P id="error"> <?php echo $_GET['error']; 
                        echo ('<meta http-equiv="refresh" content="3;url=Add-videos.php">');
                  ?>  </P> <?php endif;?>
                 
                  
                </div><!-- /.box-header -->
                                  
               
                <!-- form start -->
                <form action="Add-videos.php" method="post" style="width: 50%;margin: 0 auto;">
                  <div class="box-body">
                 
	                <div class="form-group">
                      <label for="caption">Video caption</label>
                      <input type="text" name="video_caption" class="form-control" placeholder="Enter video caption..."/>
                    </div>

                 	<div class="form-group">
                      <label for="caption">Video embed code</label>
                      <input type="text" name="video_code" class="form-control" placeholder="Enter Code..."/>
                    </div>
  
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" name="video_submit" value="new_store" class="btn btn-primary">Add Video</button>
                  </div>
                </form>
              </div><!-- /.box -->
                 
                </div><!-- /.col -->

                 
                 
                  </div><!-- /.col -->
                
              
              </div><!-- /.row -->
            </div><!-- /.box-body -->
           
          </div><!-- /.box -->

         
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    
<?php
if(isset($_POST['video_submit'])){
	
	    //$code = $_POST['video_code'];
    $code = mysqli_real_escape_string($db->concern, $_POST['video_code']);
       // $caption = $_POST['video_caption'];
       $caption =  mysqli_real_escape_string($db->concern, $_POST['video_caption']);
	  
if($code == '' || $caption == ''){
	
	
	$error ="Please Fill all the required fields";
	header('location:Add-videos.php?error='.$error);
		}else{
			

	
	$query = "INSERT videos (video_caption, embed_code) VALUES ('$caption', '$code')";
	
	$confirmation = $db->insert($query);
	
	if(isset($confirmation)){
		
		$msg = "The video has been added";
		
		header('location:Add-videos.php?msg='.$msg);
		
    }
  }       
}
include 'includes/footer.php';     

?>