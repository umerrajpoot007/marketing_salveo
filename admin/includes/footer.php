  <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://technowise360.com" target="_blank">Technowise360</a>.</strong> All rights reserved.
      </footer>

     <!--        immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
 
   
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
   
    <!-- AdminLTE for demo purposes -->
 
  </body>
</html>
<?php
if(isset($_POST['sign_out'])){
	        session_destroy();
	        header('location:index.php');}
?>