<?php
session_start();
include '../config/config.php';
include '../liberaries/database.php';
ob_start();
$db= new database();

if(isset($_SESSION['id'])){
	
	$id = $_SESSION['id'];
	
}else{
	
	header('location:index.php');
}
include 'includes/header.php';
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Add New Banner
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Coupons</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
           
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                 
                                  <div class="box box-primary">
                                  
                                   <div class="box-header with-border">
                  
                  <?php if(isset($_GET['msg'])):?> <P id="message"> <?php echo $_GET['msg'];                         echo ('<meta http-equiv="refresh" content="3;url=Add-banners.php">');
                  ?>
                  </P> <?php endif;?>

                  <?php if(isset($_GET['error'])):?> <P id="error"> <?php echo $_GET['error']; 
                        echo ('<meta http-equiv="refresh" content="3;url=Add-banners.php">');
                  ?>  </P> <?php endif;?>
                 
                  
                </div><!-- /.box-header -->
                                  
               
                <!-- form start -->
                <form action="Add-banners.php" method="post" enctype="multipart/form-data" style="width: 50%;margin: 0 auto;">
                  <div class="box-body">
                 
	                <div class="form-group">
                      <label for="caption">Upload Banner</label>
                      <input type="file" name="file" class="form-control"/>
                    </div>
            <div style="width: 100%">
                 	<div class="form-group" style="width: 12%; float: left; ">
                      <label for="caption">Width</label>
                      <input type="number" name="wdth" class="form-control"/>
                    </div>

                  <div class="form-group" style="width: 12%; float: left;margin-left: 10%">
                      <label for="caption">Height</label>
                      <input type="number" name="hight" class="form-control"/>
                    </div>
            </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <!--<button type="submit" name="new_banner" value="new_store" class="btn btn-primary">Add Banner</button>-->
                    <input type="submit" class="btn btn-primary" name="new_banner" value="Add Banner">
                  </div>
                </form>
              </div><!-- /.box -->
                 
                </div><!-- /.col -->

                 
                 
                  </div><!-- /.col -->
                
              
              </div><!-- /.row -->
            </div><!-- /.box-body -->
           
          </div><!-- /.box -->

         
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php
if(isset($_POST['new_banner'])){
	
	 
    $width  =      $_POST['wdth'];
    $height =      $_POST['hight'];

    $temp   =      explode(".", $_FILES["file"]["name"]);
    $newfilename = "banner_".rand().'.'.end($temp);

	  
if($height == '' || $width == '' || $newfilename == ''){
	
	
	$error ="Please Fill all the required fields";
	header('location:Add-banners.php?error='.$error);
		}else{
			

      $target_dir = "uploads/banners/";
      $target_file = $target_dir.$newfilename;
      $imageFileType = $_FILES["file"]["type"];
      $imageFilesize = $_FILES["file"]["size"];
// Check file size
if ($imageFilesize > 5000000) {
      $largefile = "Sorry, your file is too large.";
      header('location:Add-banners.php?storemsg='.urlencode($largefile));
  exit();
}else{
// Allow certain file formats
if($imageFileType == "image/jpg" || $imageFileType == "image/png" || $imageFileType == "image/jpeg"
|| $imageFileType == "image/gif" ) {
    
    move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);           

 $newbanner ="INSERT INTO banners (banner_name,width,height)
VALUES ('$newfilename','$width','$height')";

 $bannerinserted = $db->insert($newbanner);
 if(isset($bannerinserted)){  
 $storemsg2="New Banner has been added.";
 
 
 header('location:Add-banners.php?msg='.urlencode($storemsg2));
exit(); 

             }
    }else{
       
       $wrongformate = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
  
  header('location:Add-banners.php?storemsg='.urlencode($wrongformate));
  exit();
          }
       } 

  }       
}
include 'includes/footer.php';
?>