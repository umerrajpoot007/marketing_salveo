<?php
session_start();
include '../config/config.php';
include '../liberaries/database.php';
include '../helpers/format_helper.php';
ob_start();
$db= new database();

if(isset($_SESSION['id'])){
	
	$id = $_SESSION['id'];
	
}else{
	
	header('location:index.php');
}
include 'includes/header.php';
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Add New Photo
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add New Photo</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
         
			  <div class="col-md-12">
                 <div class="box box-primary">
                <div class="box-header with-border">
                  <!--<h3 class="box-title">Add New Store</h3>-->
                  
                <?php if(isset($_GET['storemsg'])):?> <P id="error"> <?php echo $_GET['storemsg'];
                       echo ('<meta http-equiv="refresh" content="3;url=Add-photo.php">');

                ?>  </P> <?php endif;?>  
                <?php if(isset($_GET['storecreated'])):?> <P id="message"> <?php echo $_GET['storecreated'];
                       echo ('<meta http-equiv="refresh" content="3;url=Add-photo.php">');
                ?>  </P>  <?php endif;?>
                
                
                </div><!-- /.box-header -->
                <!-- form start -->
                <form id="form3" action="Add-photo.php" method="post" enctype="multipart/form-data" style="width: 50%;margin: 0 auto;">
                  <div class="box-body">
		
				  
                  <div class="form-group" >
                    <label>Select Category</label>
                      <select class="form-control select2" name="cate" style="width: 100%;">
                      <?php 
                        $get = "SELECT * FROM categories where parent_id = 1 ORDER BY  categories ASC";

                         $options = $db->select($get);
                      	 while($array = $options -> fetch_assoc()) :
                      ?>      
                         <option selected="selected" value="<?php echo $array['category_id'];  ?>"><?php echo $array['categories'];  ?></option>
                      <?php
                      endwhile;
                      ?>                      
                      </select>
                   </div><!-- /.form-group -->
                  
					          <div class="form-group">
                       <label>Select photo Type</label>            
                      <select class="form-control" name="photo_type" style="width: 100%;">  
                          <?php 
                               $get = "SELECT * FROM photo_types ORDER BY type_name ASC";
                               $options = $db->select($get);   
                             while($array = $options -> fetch_assoc()) :  ?>            
                              <option  value="<?php echo $array['photo_type_id'];  ?>"><?php echo $array['type_name'];  ?></option>
                          <?php  endwhile; ?>                   
                      </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Photo caption</label>
                        <input type="text" name="photo_caption" class="form-control"  placeholder="Enter photo caption..."/>
                    </div>
				
      					     <div class="form-group">
                            <label>Upload Photo</label>
                            <input type="file" id="exampleInputFile" name="file" class="btn btn-primary">
                     </div>
										
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" name="Photo_form" value="New_photo" class="btn btn-primary">Add new photo</button>
                  </div>
                </form>
              </div><!-- /.box -->
                 
                </div><!-- /.col -->
			
			
            <!-- right column -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php
//main function bracket starts
if(isset($_POST['Photo_form'])){
ob_start();	

	$category      = $_POST['cate'];
	$photo_type    = $_POST['photo_type'];
	$photo_caption     = $_POST['photo_caption'];

  $temp = explode(".", $_FILES["file"]["name"]);
  $newfilename = "photo_".round(microtime(true)) . '.' . end($temp);


 // $filename = basename($_FILES["file"]["name"]);
	if($category == '' || $photo_type =='' || $photo_caption == '' || $newfilename ==''){

      $storemsg = "Please Fill all the require fields";
      header('location:Add-photo.php?storemsg='.urlencode($storemsg));
      exit();
}else{	
// Folder Name where the file is being uploaded
      $target_dir = "uploads/photos/";
      $target_file = $target_dir.$newfilename;
      $imageFileType = $_FILES["file"]["type"];
      $imageFilesize = $_FILES["file"]["size"];
// Check file size
if ($imageFilesize > 5000000) {
      $largefile = "Sorry, your file is too large.";
      header('location:Add-Category.php?storemsg='.urlencode($largefile));
  exit();
}else{
// Allow certain file formats
if($imageFileType == "image/jpg" || $imageFileType == "image/png" || $imageFileType == "image/jpeg"
|| $imageFileType == "image/gif" ) {
    
    move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);           


	
$newphoto ="INSERT INTO photos (category_id,sub_category_id,type_id,caption,photo_name)
VALUES ('1','$category','$photo_type','$photo_caption','$newfilename')";

 $photoinserted = $db->insert($newphoto);
 if(isset($photoinserted)){  
 $storemsg2="New photo has been created.";
 
 
 header('location:Add-photo.php?storecreated='.urlencode($storemsg2));
exit(); 

             }
    }else{
		   
		   $wrongformate = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	
  header('location:Add-photo.php?storemsg='.urlencode($wrongformate));
  exit();
          }
       } 
    }   
  }
include 'includes/footer.php';
?>