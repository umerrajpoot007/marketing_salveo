<?php
session_start();
include '../config/config.php';
include '../liberaries/database.php';
ob_start();
$db= new database();

if(isset($_SESSION['id'])){
	
	$id = $_SESSION['id'];
	
}else{
	
	header('location:index.php');
}
include 'includes/header.php';
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Videos
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Manage Videos</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
         
          <div class="row">
           <?php if(isset($_GET['msg'])):?> <P id="message"> <?php echo $_GET['msg'];
                   echo ('<meta http-equiv="refresh" content="3;url=manage-videos.php">');
           ?>  </P> <?php endif;?>
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				
                 <form action="manage-coupons.php" method="post">
                  <div class="box-body">
                   <div class="form-group" >
                   
                   
               
      
    
                  <div class="box-tools">
				 
                    <div class="input-group" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button type="submit" name="search_store" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
				 
                  </div>
				   </form>
                </div><!-- /.box-header -->

			
	
 <div class="box-body table-responsive no-padding">
				 
				   <table class="table table-hover">
                    <tr>
                      <th>Video Caption</th>
                    </tr>
                  
         
         <?php
		 
	$per_page=50;
if (isset($_GET["page"])) {

$page = $_GET["page"];

}

else {

$page=1;

}

// Page will start from 0 and Multiple by Per Page
$start_from = ($page-1) * $per_page;	 
	
	
		$table = "SELECT * FROM videos LIMIT $start_from, $per_page";

	$results      = $db ->select($table);
		
		if(!$results || $results->num_rows == 0){
      echo "<tr>";
      echo "<td> No data available. </td>";
      echo "</tr>";
    }else{
			
		   while($row =	$results->fetch_assoc()):
    
    
			?>

           <tr>
                      <!--<td><a href="edit-coupon.php?id=<?php// echo $row['video_id'];	 ?>" style="text-decoration:none;"><?php// echo $row['video_caption']; ?></a></td>-->

                       <td><?php echo $row['video_caption']; ?></td>

       
       <td><a href="delete.php?videoid=<?php echo $row['video_id'];?>" style="text-decoration:none;"><span class="label label-danger">Delete</span></a></td>
                       </tr>
<?php
endwhile;
}
?>
               
                  </table>
				 
                  
                </div><!-- /.box-body -->
		
              </div><!-- /.box -->
            </div>
            
            <div>
<?php

if(!$results || $results->num_rows == 0){
     
    }else{

//Now select all from table
$query = "SELECT * FROM videos";
$result = $db->select($query);

// Count the total records
$total_records = $result->num_rows;

//Using ceil function to divide the total records on per page
$total_pages = ceil($total_records / $per_page);

//Going to first page
echo "<center> <ul class='pagination'>

<li><a href='manage-videos.php?page=1'>".'First Page'."</a></li> ";

for ($i=1; $i<=$total_pages; $i++) {

echo "<li><a href='manage-videos.php?page=".$i."'>".$i."</a></li> ";
};
// Going to last page
echo "<li><a href='manage-videos.php?page=$total_pages'>".'Last Page'."</a></li></center> ";
}
?>

</div>
            
            
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	 <?php include 'includes/footer.php'; ?>