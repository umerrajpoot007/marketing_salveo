<?php
session_start();
include '../config/config.php';
include '../liberaries/database.php';
if(isset($_SESSION['id'])){
	
	$id = $_SESSION['id'];
	
}else{
	
	header('location:index.php');
}
ob_start();
$db= new database();
include 'includes/header.php';
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>

        </section>
<?php        
$phot = "SELECT * FROM photos";
$phots = $db->select($phot);
if(!$phots || $phots->num_rows == 0){
$total_photos = "Empty";
}else{
$total_photos = $phots -> num_rows;
	
	}
?>     
	    <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $total_photos; ?></h3>
                  <p>Photos</p>
                </div>
                <div class="icon">
                  <i class="ion ion-images"></i>
                </div>
                <a href="manage-photos.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
       

  <?php        
$cat = "SELECT * FROM banners";
$cats = $db->select($cat);
if(!$cats || $cats->num_rows == 0){
$total_banners = "Empty";
}else{
$total_banners = $cats -> num_rows;
  
  }
?>   

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo $total_banners; ?></h3>
                  <p>Banners</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-image"></i>
                </div>
                <a href="manage-banners.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

  <?php        
$vids = "SELECT * FROM videos";
$vides = $db->select($vids);
if(!$vides || $vides->num_rows == 0){
$total_videos = "Empty";
}else{
$total_videos = $vides -> num_rows;
  
  }
?>  

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo $total_videos; ?></h3>
                  <p>Videos</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-film"></i>
                </div>
                <a href="manage-videos.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
           
          
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php include 'includes/footer.php'; ?>