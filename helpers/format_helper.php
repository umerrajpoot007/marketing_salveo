<?php
function titleToLink($url)
{
    # Prep string with some basic normalization
    $url = strtolower($url);
    $url = strip_tags($url);
    $url = stripslashes($url);
    $url = html_entity_decode($url);

    # Remove quotes (can't, etc.)
    $url = str_replace('\'', '', $url);

    # Replace non-alpha numeric with hyphens
    $match = '/[^a-z0-9]+/';
    $replace = '-';
    $url = preg_replace($match, $replace, $url);

    $url = trim($url, '-');

    return $url;
}
//fnction for category link


function dateformat($date){

return date('F j, Y, g:i s a',strtotime($date));

}

function textshortener ($text,$length = 500){


    $text = substr($text , 0 ,$length);
 $text = substr($text , 0, strrpos($text,' '));

    $text = $text."....";
    return $text;
}

function profileLink($title)
{
 return base_url.'photos/'.titleToLink($title);
}
/*
function catepath($cat,$id){

 return base_url.titleToLink($cat).'/'.$id;
}


function pagedire($id,$name){

 return base_url.'info/'.$id.'/'.titleToLink($name);
}


function offerpage($groupid,$groupname){

 return base_url.'Coupons/'.$groupid.'/'.titleToLink($groupname);
}
*/
?>