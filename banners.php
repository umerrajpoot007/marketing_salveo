<?php include 'includes/header.php'; ?>

<?php 
ob_start();
$db= new database();

  $all_banners = "SELECT * FROM banners";
  $grab_banner = $db->select($all_banners);
 
?>
        <section id="content-wrap" class="row">
            <div class="col-md-10 col-md-offset-1 fullheight">
                <!-- Beginning of about section -->
                <main class="albums content mayScroll setheight row" data-mcs-theme="dark">
                    <div class="col-md-12">
                        <div class="row">
                            <ul class="gallery-container list-unstyled">
                                <?php
                        if(!$grab_banner || $grab_banner->num_rows == 0){
                                ?>
                                <li class="album-set-img col-md-6">
                                    <a href="<?php echo base_url; ?>assets/images/image-not-available.jpg">
                                        <img src="<?php echo base_url; ?>assets/images/image-not-available.jpg" alt="Title" title="Title" class="img-responsive"/> 
                                    </a>
                                </li>
                                 <li class="album-set-img col-md-6">
                                    <a href="<?php echo base_url; ?>assets/images/image-not-available.jpg">
                                        <img src="<?php echo base_url; ?>assets/images/image-not-available.jpg" alt="Title" title="Title" class="img-responsive"/> 
                                    </a>
                                </li>
                                <?php
                                  }else{
                                         while($banner_pic = $grab_banner->fetch_assoc()):
                                    ?>
                                <li class="album-set-img col-md-6" style="max-height: 460px;">
                                    <a href="<?php echo base_url; ?>admin/uploads/banners/<?php echo $banner_pic['banner_name']; ?>">
                                        <img src="<?php echo base_url; ?>admin/uploads/banners/<?php echo $banner_pic['banner_name']; ?>" alt="Title" title="" class="img-responsive" style="min-height: 365px !important;max-height: 365px !important;/*min-width: 613px !important;*/width: 100%;"/> 
                                    </a>
                                    <div style="width: 100%;height: 100%;" id="divid" contenteditable="true" onclick="document.execCommand('selectAll',false,null)">
                                        <code style="width: 100%; float:left;width: 100%;overflow:scroll">

                                        &lt;div class="Row"&gt;    &lt;img src="<?php echo base_url; ?>admin/uploads/banners/<?php echo $banner_pic['banner_name']; ?>" style="width:<?php echo $banner_pic['width']."px"; ?>;height:<?php echo $banner_pic['height']."px";?>" &gt; &lt;/div&gt;
                                        </code>
                                    </div>
                                </li>
                            <?php
                            endwhile;
                              }
                                ?>
                            </ul>
                        </div>
                    </div>
                </main>
            </div>
        </section>
<?php include 'includes/footer.php'; ?>  