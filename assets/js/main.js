/*jshint undef:false */
(function (window, document, $) {
    "use strict";

    var isMobile = false;

    window.$ = window.$ = $;

    $(document).ready(function () {
        "use strict";

        var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
        $("#menu-container a").each(function () {
            if ($(this).attr("href") == pgurl || $(this).attr("href") == '') {
                $(this).addClass("active");
            }
        });


        if ($('.gallery-container').length) {
            $('.gallery-container').magnificPopup({
                delegate: 'a', // child items selector, by clicking on it popup will open
                type: 'image',
				// other options
                gallery: {
                    enabled: true
                }
            });
        }


        /* Send contact form to email */
        $('#contactform').on('submit', function (e) {
			var form_data = $(this).serialize() + '&submit=send';
            $('body').addClass('sending');
			e.preventDefault();

            window.setTimeout(function () {
                $.ajax({
                    type: "POST",
                    url: "contacts.php", // Path to contact_form.php
                    data: form_data,
                    dataType: 'jsonp'
                })
                .done(function (data) {
                    $('#callback').html(data.msg).show('slow');
                    if (data.status) {
                        $('#contactform').find('input[type=text], textarea, select').val('');
                    }
                });

                $('body').removeClass('sending');

            }, 1500);

            return false;
        });


        if ($('.banner').length) {
            $('.banner').unslider({
                speed: 500, 				//  The speed to animate each slide (in milliseconds)
                delay: 6000, 				//  The delay between slide animations (in milliseconds)
                complete: function () { 	//  A function that gets called after every slide animation

				},
                fluid: true
            });

            $('body').addClass('hasSlider');
            $('.banner').height('initial');
        }

        /* Send Planning Form to email */
        $('#planningform').on('submit', function (e) {
			var form_data = $(this).serialize() + '&submit=send';
            $('body').addClass('sending');
			e.preventDefault();

            window.setTimeout(function () {

                $.ajax({
                    type: "POST",
                    url: "planning.php", 	// Path to contact_form.php
                    data: form_data,
                    dataType: 'jsonp'
                })
                .done(function (data) {
                    $('#callback').html(data.msg).show('slow');
                    if (data.status) {
                        $('#planningform').find('input[type=text], textarea, select').val('');
                    }
                });
                $('body').removeClass('sending');

            }, 1500);

            return false;
        });

    });

    function cheickIsMobile() {
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $('html').addClass('ismobile');
           isMobile = true;
        }
    }

    function onResize() {
        var windowW = $(window).width();
        var windowH = $(window).height();
        var menuHeight = $('header.top-menu').outerHeight(true);
        var footerHeight = $('#footer').outerHeight(true);
        var wrapperHeight = parseInt(windowH * 0.86, 10);
        var marginTop = 0;
        var contentMinHeight = 300;
        var contentMinWidth = 768;
        var contentHeight = wrapperHeight - footerHeight -  menuHeight;

        // reset #wrapper margin
        $('#wrapper, .setheight').attr('style', '');
        $('.setheight').removeClass('mCS_no_scrollbar_y');

		// checking if content is heigher than window size
        if ( windowW < contentMinWidth || contentHeight < contentMinHeight) {
			// if so, set maxHeight to 100% and remove mCustomeScrollbar from it
            $(".setheight").mCustomScrollbar("destroy");
            // reset body styles
            $("body")
                .addClass('noCustomScroll')
                .attr('style', '');

            return;
        }

		// check if window height is bigger than 568px
        if ( windowW > 767 && !isMobile ) {
			// if so :
			// set mCustomeScrollbar to the .setheight
            $(".setheight.mayScroll")
                .mCustomScrollbar({
                    timeout: 0,
                    scrollInertia: 0,
                    callbacks:{
                        onOverflowYNone: function() {
                            console.log('no scroll ...');
                            $('.setheight').addClass('mCS_no_scrollbar_y');
                        }
                    }
                });

            $("body").css("overflow-y", 'hidden');
            // set calculated height to #wrapper
            $('#wrapper').height(wrapperHeight);
			// set calculated height to #content-wrap
            $('.setheight').height(contentHeight);

			// calc the marginTop of the #wrapper to center the content in the middle of the window
            marginTop = (windowH - wrapperHeight) / 2;
            marginTop = marginTop < 0 ? 0 : marginTop;

			// set the calculated marginTop to #wrapper
            $('#wrapper').css({
                marginTop: parseInt(marginTop, 10)
            });
        }
    }

    function sliderCentering () {
        if ($('.banner').length) {
            $('.banner').css({
                'marginTop': 0,
                'marginBottom': 0
            });

            var windowH = isMobile ? $(window).height():$('.container-fluid').height();
            var menuHeight = $('header.top-menu').outerHeight(true);
            var footerHeight = $('#footer').outerHeight(true);
            var slideHeight = $('.banner li img').first().height();
            var margin = windowH-menuHeight-footerHeight-slideHeight;

            if( margin>0 ) {
                $('.banner').css({
                    'marginTop': parseInt((margin)/2, 10),
                    'marginBottom': parseInt((margin)/2, 10)
                });
            }
        }
    }

    $(window).on("load", function () {
        cheickIsMobile();
        onResize();
    });

    $(window).on("resize", function () {
        cheickIsMobile();
        onResize();
    });

})(window, document, jQuery);
